Document: python-github3.py
Title: Debian python-github3.py Manual
Author: <insert document author here>
Abstract: This manual describes what python-github3.py is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/python-github3.py/python-github3.py.sgml.gz

Format: postscript
Files: /usr/share/doc/python-github3.py/python-github3.py.ps.gz

Format: text
Files: /usr/share/doc/python-github3.py/python-github3.py.text.gz

Format: HTML
Index: /usr/share/doc/python-github3.py/html/index.html
Files: /usr/share/doc/python-github3.py/html/*.html
